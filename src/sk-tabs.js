
import { SkElement, SELECTED_AN, DISABLED_AN, TPL_PATH_AN } from '../../sk-core/src/sk-element.js';
import { DIMPORTS_AN } from "../../sk-core/src/sk-config.js";
import { ResLoader } from "../../sk-core/complets/res-loader.js";

export const TAB_TN = "sk-tab";

export const TITLE_AN = "title";
export const OPEN_AN = "open";

export const TAB_TN_AN = "tab-tn";

export const SLOTTED_TPLS_AN = "slotted-tpls";

import { SkSltabsUtil } from "./sk-sltabs.js";



export class SkTabs extends SkElement {

    get cnSuffix() {
        return 'tabs';
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }
    
    get selected() {
        return this._selected;
    }
    
    set selected(idx) {
        this._selected = idx;
        this.selectTab(idx);

        this.setAttribute(SELECTED_AN, idx);
    }
    
    get tabsSlot() {
        return this.el.querySelector('#tabsSlot');
    }
    
    get panelsSlot() {
        return this.el.querySelector('#panelsSlot');
    }
    
    get tabs() {
        return this.tabsSlot.assignedNodes({ flatten: true });
    }
    
    get panels() {
        return this.panelsSlot.assignedNodes({ flatten: true }).filter(el => {
              return el.nodeType === Node.ELEMENT_NODE;
        });
    }
            
    get tabTn() {
        return this.getAttribute(TAB_TN_AN) || TAB_TN;
    }

    set tabTn(tabTn) {
        return this.setAttribute(TAB_TN_AN, tabTn);
    }
        
    get tabSl() {
        return this.tabTn;
    }
    
    get disabled() {
        return this.hasAttribute(DISABLED_AN);
    }
    
    get utilClassName() {
        return 'SkSltabsUtil';
    }
    
    get utilPath() {
        return '/node_modules/sk-tabs/src/sk-sltabs.js';
    }
    
    get slUtil() {
        if (! this._slUtil) {
			if (! window.SkSltabsUtil) {
				window.SkSltabsUtil = SkSltabsUtil;
			}
            this._slUtil = 
                ResLoader.dynLoad(this.utilClassName, this.utilPath, function(def) {
                    return new def();
                }.bind(this), false, false, false ,false, false, false, true, true);
        
        } 
        return this._slUtil; 
    }
    
    attributeChangedCallback(name, oldValue, newValue) {
        if (name === DISABLED_AN && oldValue !== newValue) {
            if (newValue) {
                if (this.impl && typeof this.impl.disable === 'function') {
                    this.impl.disable();
                }
            } else {
                if (this.impl && typeof this.impl.enable === 'function') {
                    this.impl.enable();
                }
            }
        }
    }

    static get observedAttributes() {
        return [ DISABLED_AN ];
    }
    
    get isTplSlotted() {
        let cfgSlotted = this.confValOrDefault(SLOTTED_TPLS_AN, "false"); // no slots by default
        if (this.isInIE() || (cfgSlotted === "false")) {
            return false;
        }
        return true;
    }
    
    render() {
        if (! this.isTplSlotted) {
            super.render();
        } else {
            super.render();
            this.whenRendered(() => {
                this.setInitialAttrs();
                
                setTimeout(() => {            
                    this.unbindTabEvents();
                    this.bindTabEvents(); 
                }, 0);     
            });   

        }
    }
    
    disconnectedCallback() {
        this.unbindTabEvents();
    }
    
    setInitialAttrs() {
        return this.slUtil.setInitialAttrs.call(this);
    }
    
    bindTabEvents() {
        return this.slUtil.bindTabEvents.call(this);
    }
    
    unbindTabEvents() {
        return this.slUtil.unbindTabEvents.call(this);
    }
      
    onTitleClick(e) { 
        return this.slUtil.onTitleClick.call(this, e);
    }
      
    onKeyDown(e) {
        return this.slUtil.onKeyDown.call(this, e);
    }
    
    findFirstSelectedTab() {
        return this.slUtil.findFirstSelectedTab.call(this);
    }
      
    selectTab(idx = null) {
        return this.slUtil.selectTab.call(this, idx);
    }
         
}
