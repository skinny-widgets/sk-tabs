
import { TPL_PATH_AN, DISABLED_AN, SELECTED_AN } from "../../../sk-core/src/sk-element.js";

import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';

import { OPEN_AN, TITLE_AN } from "../sk-tabs.js";

import { SkSltabsImpl } from "./sk-sltabs-impl.js";


export class SkTabsImpl extends SkComponentImpl {
    
    get tplPath() {
        if (! this._tplPath) {
            if (this.comp.isTplSlotted) {
                let suffix = 'sltabs';
                this._tplPath = this.comp.tplPath
                    || (this.comp.configEl && this.comp.configEl.hasAttribute(TPL_PATH_AN))
                        ? `${this.comp.configEl.getAttribute(TPL_PATH_AN)}/${this.prefix}-sk-${suffix}.tpl.html`
                        :`/node_modules/sk-tabs-${this.prefix}/src/${this.prefix}-sk-${suffix}.tpl.html`;
            } else {
                this._tplPath = this.comp.tplPath
                    || (this.comp.configEl && this.comp.configEl.hasAttribute(TPL_PATH_AN))
                        ? `${this.comp.configEl.getAttribute(TPL_PATH_AN)}/${this.prefix}-sk-${this.suffix}.tpl.html`
                        :`/node_modules/sk-tabs-${this.prefix}/src/${this.prefix}-sk-${this.suffix}.tpl.html`;
            }
        }
        return this._tplPath;
    }

    get suffix() {
        return 'tabs';
    }
    
    get slImplInst() {
        if (! this._slImplInst) {
            this._slImplInst = new SkSltabsImpl();
        }
        return this._slImplInst;
    }
    
    beforeRendered() {
        super.beforeRendered();
        if (this.comp.isTplSlotted) {
            return this.slImplInst.importTabEls.call(this);
        } else {
            this.saveState();
        }
        
        
    }
    
    renderSlottedTabs(tabEls) {
        return this.slImplInst.renderTabs.call(this, tabEls);
    }

    restoreState(state) {
        this._tabsEl = null;
        this._tabsTitleBarEl = null;
        this._titleContentEl = null;
        this.tabsContentEl.innerHTML = '';
        this.titleBarEl.innerHTML = '';
        this.tabsContentEl.insertAdjacentHTML('beforeend', state.contentsState);
        this.renderTabs();
        this.bindTabSwitch();
        if (! this.titleBarEl.querySelector(`[${OPEN_AN}]`)) {
            this.updateTabs('tabs-1');
        }
    }

    afterRendered() {
        super.afterRendered();
        this.restoreState({ contentsState: this.contentsState || this.comp.contentsState });
    }

    get cachedTplId() {
        if (this.comp.isTplSlotted) {
            return 'SkSltabsTpl';
        } else {
            return this.comp.constructor.name + 'Tpl';
        }
    }
}
