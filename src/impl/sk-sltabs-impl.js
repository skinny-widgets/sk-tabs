

import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';

import { OPEN_AN, TITLE_AN } from "../sk-tabs.js";

import { TPL_PATH_AN, DISABLED_AN, SELECTED_AN } from "../../../sk-core/src/sk-element.js";


export class SkSltabsImpl extends SkComponentImpl {

    get suffix() {
        return 'sltabs';
    }

    get tplPath() {
        if (! this._tplPath) {
            this._tplPath = this.comp.tplPath
                || (this.comp.configEl && this.comp.configEl.hasAttribute(TPL_PATH_AN))
                    ? `${this.comp.configEl.getAttribute(TPL_PATH_AN)}/${this.prefix}-sk-${this.suffix}.tpl.html`
                    :`/node_modules/sk-tabs-${this.prefix}/src/${this.prefix}-sk-${this.suffix}.tpl.html`;
        }
        return this._tplPath;
    }
    
    beforeRendered() {
        super.beforeRendered();
        this.importTabEls()
    }
    
    importTabEls() {
        let tabEls = this.comp.querySelectorAll(this.comp.tabSl);
        if (tabEls && tabEls.length > 0) {
            this.renderTabs(tabEls);
        }
    }

    renderTabs(tabEls) {
        let tabs = tabEls || this.tabsEl.querySelectorAll(this.comp.tabSl);
        let num = 1;
        this.tabs = {};
        for (let tab of tabs) {
            let isOpen = tab.hasAttribute(OPEN_AN);
            let title = tab.getAttribute(TITLE_AN) ? tab.getAttribute(TITLE_AN) : '';
            let disabled = tab.hasAttribute(DISABLED_AN);
            let selected = tab.hasAttribute(SELECTED_AN) || tab.hasAttribute(OPEN_AN);
            this.comp.insertAdjacentHTML('beforeend', `
                <h2 slot="title" ${disabled ? 'disabled' : ''} ${selected ? 'selected' : ''}>${title}</h2>
                <section id="tabs-${num}">${tab.innerHTML}</section>
            `);
            this.removeEl(tab);
            this.tabs['tabs-' + num] = this.comp.querySelector('#tabs-' + num);
            num++;
        }
    }
}
