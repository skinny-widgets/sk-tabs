import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';


export class SkTabImpl extends SkComponentImpl {

    beforeRendered() {
        super.beforeRendered();
        this.saveState();
    }

    afterRendered() {
        super.afterRendered();
        this.clearAllElCache();
        this.restoreState({ contentsState: this.contentsState || this.comp.contentsState });
        this.mountStyles();
    }

    restoreState(state) {
        this.comp.el.innerHTML = state.contentsState;
    }
}