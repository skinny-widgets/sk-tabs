
import { SkElement, SELECTED_AN, DISABLED_AN, IMPL_PATH_AN } from "../../sk-core/src/sk-element.js";

import { DIMPORTS_AN } from "../../sk-core/src/sk-config.js";
import { ResLoader } from "../../sk-core/complets/res-loader.js";
import { TAB_TN, TAB_TN_AN } from "./sk-tabs.js";

export class SkSltabsUtil {
    
    setInitialAttrs() {
        this.setAttribute('role', 'tablist');
        
        for (let [i, panel] of this.panels.entries()) {
          panel.setAttribute('role', 'tabpanel');
          panel.setAttribute('tabindex', 0);
        }

        this.selected = this.findFirstSelectedTab() || 0; 
    }
    
    bindTabEvents() {
        this.titleClickHandle = this.onTitleClick.bind(this);
        this.keyDownHandle = this.onKeyDown.bind(this);
        
        this.tabsSlot.addEventListener('click', this.titleClickHandle);
        this.tabsSlot.addEventListener('keydown', this.keyDownHandle);
    }
    
    unbindTabEvents() {
        if (this.titleClickHandle) {
            this.tabsSlot.removeEventListener('click', this.titleClickHandle);
        }
        if (this.keyDownHandle) {
            this.tabsSlot.removeEventListener('keydown', this.keyDownHandle);
        }
    }
      
    onTitleClick(e) { 
        if (e.target.slot === 'title' && ! this.disabled && ! e.target.hasAttribute(DISABLED_AN)) {
            this.selected = this.tabs.indexOf(e.target);
            e.target.focus();
        }
    }
      
    onKeyDown(e) {
        switch (e.code) {
          case 'ArrowUp':
          case 'ArrowLeft':
            e.preventDefault();
            var idx = this.selected - 1;
            idx = idx < 0 ? this.tabs.length - 1 : idx;
            this.tabs[idx].click();
            break;
          case 'ArrowDown':
          case 'ArrowRight':
            e.preventDefault();
            var idx = this.selected + 1;
            this.tabs[idx % this.tabs.length].click();
            break;
          default:
            break;
        }
    }
    
    findFirstSelectedTab() {
        let selectedIdx;
        for (let [i, tab] of this.tabs.entries()) {
            tab.setAttribute('role', 'tab');
        
            if (tab.hasAttribute(SELECTED_AN)) {
                selectedIdx = i;
            }
        }
        return selectedIdx;
    }
      
    selectTab(idx = null) {
        for (let i = 0, tab; tab = this.tabs[i]; ++i) {
            let select = i === idx;
            tab.setAttribute('tabindex', select ? 0 : -1);
            tab.setAttribute('aria-selected', select);
            this.panels[i].setAttribute('aria-hidden', !select);
        }
    }    
}





export class SkSltabs extends SkElement {

    get cnSuffix() {
        return 'sltabs';
    }
    
    get implPath() {
        return this.getAttribute(IMPL_PATH_AN) ||
            `/node_modules/sk-tabs-${this.theme}/src/${this.theme}-sk-${this.cnSuffix}.js`;
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }
        
    get utilClassName() {
        return 'SkSltabsUtil';
    }
    
    get utilPath() {
        return '/node_modules/sk-tabs/src/sk-sltabs.js';
    }
    
    get slUtil() {
        if (! this._slUtil) {
            window.SkSltabsUtil = SkSltabsUtil;
            this._slUtil = 
                ResLoader.dynLoad(this.utilClassName, this.utilPath, function(def) {
                    return new def();
                }.bind(this), false, false, false ,false, false, false, true, true);
        
        } 
        return this._slUtil; 
    }
    
    get selected() {
        return this._selected;
    }
    
    set selected(idx) {
        this._selected = idx;
        this.selectTab(idx);

        this.setAttribute(SELECTED_AN, idx);
    }
    
    get tabsSlot() {
        return this.el.querySelector('#tabsSlot');
    }
    
    get panelsSlot() {
        return this.el.querySelector('#panelsSlot');
    }
    
    get tabs() {
        return this.tabsSlot.assignedNodes({ flatten: true });
    }
    
    get panels() {
        return this.panelsSlot.assignedNodes({ flatten: true }).filter(el => {
              return el.nodeType === Node.ELEMENT_NODE;
        });
    }
            
    get tabTn() {
        return this.getAttribute(TAB_TN_AN) || TAB_TN;
    }

    set tabTn(tabTn) {
        return this.setAttribute(TAB_TN_AN, tabTn);
    }
        
    get tabSl() {
        return this.tabTn;
    }
    
    get disabled() {
        return this.hasAttribute(DISABLED_AN);
    }
    
    render() {
        super.render();

        this.whenRendered(() => {
            this.setInitialAttrs();
            
            setTimeout(() => {            
                this.unbindTabEvents();
                this.bindTabEvents(); 
            }, 0);     
        });     
    }
    
    disconnectedCallback() {
        this.unbindTabEvents();
    }
    
    setInitialAttrs() {
        return this.slUtil.setInitialAttrs.call(this);
    }
    
    bindTabEvents() {
        return this.slUtil.bindTabEvents.call(this);
    }
    
    unbindTabEvents() {
        return this.slUtil.unbindTabEvents.call(this);
    }
      
    onTitleClick(e) { 
        return this.slUtil.onTitleClick.call(this, e);
    }
      
    onKeyDown(e) {
        return this.slUtil.onKeyDown.call(this, e);
    }
    
    findFirstSelectedTab() {
        return this.slUtil.findFirstSelectedTab.call(this);
    }
      
    selectTab(idx = null) {
        return this.slUtil.selectTab.call(this, idx);
    }
      
    attributeChangedCallback(name, oldValue, newValue) {
        if (name === DISABLED_AN && oldValue !== newValue) {
            if (newValue) {
                if (this.impl && typeof this.impl.disable === 'function') {
                    this.impl.disable();
                }
            } else {
                if (this.impl && typeof this.impl.enable === 'function') {
                    this.impl.enable();
                }
            }
        }
    }

    static get observedAttributes() {
        return [ DISABLED_AN ];
    }
}
